class CreateUsers < ActiveRecord::Migration
  def up
  end
  
  def change
    create_table :users do |t|
		t.datetime "created_at"
		t.datetime "updated_at"
		t.string   "email"
		t.string   "alias"
		t.integer  "btcjam_uid"
		t.string   "btcjam_access_token"
		t.text     "oauth_data"
    end
  end

  def down
  end
end
